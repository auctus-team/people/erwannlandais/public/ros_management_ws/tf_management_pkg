#include <tf_management_pkg/tf_utils.h>

tfUtils::tfUtils(ros::NodeHandle & nh,bool canGetPose):
    tfBuffer(new tf2_ros::Buffer(ros::Duration(3.0))),
    tfListener(new tf2_ros::TransformListener(*tfBuffer) )
{
    this->canGetPose = canGetPose;

    nh_ = nh;    
    if (!canGetPose)
    {
        delete tfBuffer;
        delete tfListener;

    }

}

bool tfUtils::getPose(std::string refG, std::string refTrack, std::vector<double> & poseGoal, int tries)
{

    int k = 0;
    bool success = false;
    if (canGetPose)
    {
        while (nh_.ok() && !success and k < tries)
        {
            try{
                geometry_msgs::TransformStamped tlookup;
                tlookup = tfBuffer->lookupTransform(refG, refTrack, ros::Time(0), ros::Duration(1e-4));

                poseGoal = {
                tlookup.transform.translation.x,
                tlookup.transform.translation.y,
                tlookup.transform.translation.z,
                tlookup.transform.rotation.x,
                tlookup.transform.rotation.y,
                tlookup.transform.rotation.z,
                tlookup.transform.rotation.w                                                                                  
                };
                success = true;
            }

            catch (tf2::TransformException &ex) {
                k+=1;
            }
        }
    }
    return(success);
}

bool tfUtils::getPose(std::string refG, std::string refTrack, geometry_msgs::Pose & poseGoal, int tries)
{
    std::vector<double> L;
    bool suc = getPose(refG,refTrack, L, tries);
    if (suc)
    {
        poseGoal = toP(L);
    }
    return(suc);
}
        
bool tfUtils::getPose(std::string refG, std::string refTrack, geometry_msgs::Transform & poseGoal, int tries)
{
    std::vector<double> L;
    bool suc = getPose(refG,refTrack, L, tries);
    if (suc)
    {
        poseGoal = toT(L);
    }
    return(suc);
}

bool tfUtils::getPose(std::string refG, std::string refTrack, Eigen::Matrix4f & poseGoal, int tries)
{
    std::vector<double> L;
    bool suc = getPose(refG,refTrack, L, tries);
    if (suc)
    {
        poseGoal = toMf(L);
    }
    return(suc);
}

bool tfUtils::getPose(std::string refG, std::string refTrack, Eigen::Matrix4d & poseGoal, int tries)
{
    std::vector<double> L;
    bool suc = getPose(refG,refTrack, L, tries);
    if (suc)
    {
        poseGoal = toM(L);
    }
    return(suc);
}

std::vector<double> tfUtils::toL(Eigen::Matrix4d O)
{
    Eigen::Matrix3d M;
    M.block<3,3>(0,0) = O.block<3,3>(0,0);
    Eigen::Quaternion<double> q(M);
    std::vector<double> L;
    L.push_back(O(0,3));
    L.push_back(O(1,3));    
    L.push_back(O(2,3));    
    L.push_back(q.x());
    L.push_back(q.y());
    L.push_back(q.z());
    L.push_back(q.w());                
    return(L);
}

std::vector<double> tfUtils::toL(Eigen::Matrix4f O)
{
    Eigen::Matrix3f M;
    M.block<3,3>(0,0) = O.block<3,3>(0,0);
    Eigen::Quaternion<float> q(M);
    std::vector<double> L;
    L.push_back(O(0,3));
    L.push_back(O(1,3));    
    L.push_back(O(2,3));    
    L.push_back(q.x());
    L.push_back(q.y());
    L.push_back(q.z());
    L.push_back(q.w());                
    return(L);
}


std::vector<double> tfUtils::toL(geometry_msgs::Transform O)  
{
    std::vector<double> L = {O.translation.x,
        O.translation.y,
        O.translation.z,
        O.rotation.x,
        O.rotation.y,
        O.rotation.z,    
        O.rotation.w};
    return(L);
}
std::vector<double> tfUtils::toL(geometry_msgs::Pose O)
{
    std::vector<double> L = {O.position.x,
        O.position.y,
        O.position.z,
        O.orientation.x,
        O.orientation.y,
        O.orientation.z,    
        O.orientation.w};
    return(L);
}     

Eigen::Matrix4d tfUtils::toM(std::vector<double> O)
{
    Eigen::Matrix4d M; M.setIdentity();
    Eigen::Quaternion<double> q(O[6],O[3],O[4],O[5]);
    M.block<3,3>(0,0) = q.matrix();
    M(0,3) = O[0];
    M(1,3) = O[1];
    M(2,3) = O[2];
    return(M);

}

Eigen::Matrix4f tfUtils::toMf(std::vector<double> O)
{
    Eigen::Matrix4f M; M.setIdentity();
    Eigen::Quaternion<float> q(O[6],O[3],O[4],O[5]);
    M.block<3,3>(0,0) = q.matrix();
    M(0,3) = O[0];
    M(1,3) = O[1];
    M(2,3) = O[2];
    return(M);

}

geometry_msgs::Pose tfUtils::toP(std::vector<double> O)
{
    geometry_msgs::Pose pose;
        pose.position.x = O[0];
        pose.position.y = O[1];
        pose.position.z = O[2];
        pose.orientation.x = O[3];
        pose.orientation.y = O[4];
        pose.orientation.z = O[5] ;   
        pose.orientation.w = O[6] ;     
    return(pose);    
}  

geometry_msgs::Transform tfUtils::toT(std::vector<double> O)
{
    geometry_msgs::Transform tlookup;
    tlookup.translation.x = O[0];
    tlookup.translation.y = O[1];
    tlookup.translation.z = O[2];
    tlookup.rotation.x = O[3];
    tlookup.rotation.y = O[4];
    tlookup.rotation.z = O[5] ;   
    tlookup.rotation.w = O[6] ;     
    return(tlookup);
}
