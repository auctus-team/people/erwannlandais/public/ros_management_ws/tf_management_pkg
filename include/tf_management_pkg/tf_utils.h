#include <ros/ros.h>

#include <tf2_ros/transform_listener.h>
#include <eigen3/Eigen/Core>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Pose.h>
#include <eigen3/Eigen/Geometry>

#include <typeinfo>

class tfUtils
{
    public:
        tfUtils(ros::NodeHandle & nh,bool canGetPose = false);

        std::vector<double> toL(Eigen::Matrix4d O);     
        std::vector<double> toL(Eigen::Matrix4f O);        
        std::vector<double> toL(geometry_msgs::Transform O);     
        std::vector<double> toL(geometry_msgs::Pose O);   


        Eigen::Matrix4d toM(std::vector<double> L);
        template <class T>
        Eigen::Matrix4d toM(T O)
        {
            return(toM(toL(O)));
        }

        Eigen::Matrix4f toMf(std::vector<double> L);
        template <class T>
        Eigen::Matrix4f toMf(T O)
        {
            return(toMf(toL(O)));
        }        

        geometry_msgs::Pose toP(std::vector<double> O);
        template <class T>
        geometry_msgs::Pose toP(T O)
        {
            return(toP(toL(O)));
        }


        geometry_msgs::Transform toT(std::vector<double> O);
        template <class T>
        geometry_msgs::Transform toT(T O)
        {
            return(toT(toL(O)));
        } 

        bool getPose(std::string refG, std::string refTrack, std::vector<double> & poseGoal, int tries = 10);
        
        bool getPose(std::string refG, std::string refTrack, Eigen::Matrix4d & poseGoal, int tries = 10);

        bool getPose(std::string refG, std::string refTrack, Eigen::Matrix4f & poseGoal, int tries = 10);

        bool getPose(std::string refG, std::string refTrack, geometry_msgs::Pose & poseGoal, int tries = 10);

        bool getPose(std::string refG, std::string refTrack, geometry_msgs::Transform & poseGoal, int tries = 10);


        tf2_ros::Buffer * tfBuffer;
        tf2_ros::TransformListener * tfListener;
        ros::NodeHandle nh_;

        bool canGetPose;
        
};
