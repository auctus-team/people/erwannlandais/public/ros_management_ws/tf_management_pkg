#!/usr/bin/env python3

import rospy
import tf2_ros
import geometry_msgs.msg
import yaml
import rospkg


def loadTransformFromYaml(yaml_path):
    transform = geometry_msgs.msg.TransformStamped()
    with open(yaml_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        transform.header.stamp = rospy.Time.now()
        transform.header.frame_id = data["header"]["frame_id"]
        transform.child_frame_id = data["header"]["child_frame_id"]
        transform.transform.translation.x = data["position"]["x"]
        transform.transform.translation.y = data["position"]["y"]
        transform.transform.translation.z = data["position"]["z"]
        transform.transform.rotation.x = data["orientation"]["x"]
        transform.transform.rotation.y = data["orientation"]["y"]
        transform.transform.rotation.z = data["orientation"]["z"]
        transform.transform.rotation.w = data["orientation"]["w"]
    return transform


if __name__ == "__main__":
    # Initialize node and objects
    rospy.init_node("optitrack_tf_static_publisher")
    rate = rospy.Rate(10.0)
    rospack = rospkg.RosPack()
    static_broadcaster = tf2_ros.StaticTransformBroadcaster()

    yaml_name = rospy.get_param("~yaml_tf", "")
    if yaml_name == "":
        yaml_name = rospack.get_path("optitrack_to_robot") + "/config/world_to_world_optitrack.yaml"
    else:
        yaml_name+= ".yaml"
    # Load calibration tag position relatively to the world
    tf_world_Hworld_optitrack = loadTransformFromYaml(yaml_name
    )

    static_broadcaster.sendTransform(tf_world_Hworld_optitrack)
    rospy.spin()
