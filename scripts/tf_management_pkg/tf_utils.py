import numpy as np
import rospy

import tf.transformations
import geometry_msgs.msg
import tf2_ros

from msg_srv_action_gestion.srv import SetStringWR

import geometry_msgs.msg

import yaml

class tfUtils():

    def __init__(self, canGetPose = False, canProposeServices = False):
        """!
        
        canGetPose : will active the getPose() function.
        Is desactivate normally (to save processing time, if you just need the different conversion)

        canProposeServices : will activate different usefull services.
        Again, is normally desactivate to save processing time.
        
        """
        if canGetPose:
            self.tfBuffer = tf2_ros.Buffer()
            self.listener = tf2_ros.TransformListener(self.tfBuffer)
            self.allstBR = {}
            if (canProposeServices):
                self.turnToStatic = rospy.Service('to_static_tf',SetStringWR, self.tfToStaticSrv)


        self.canProposeServices = canProposeServices
        self.canGetPose = canGetPose

        self.blockDicMod = False

        self.lastHeaderTime = -1

    def tfToStatic(self,transf,L):
        """!
        
        len(L) = 2 : L = [parent,child]. child_frame_id of static transform will be called child_st
        len(L) = 3 : L = [parent,child,child_frame_id_of_static_transform]

        You can use this function to create a new transform(parent => child) with this : 

        [parent," ",child]

        Second element of the list is only used as a way to save the transform as a specific key.
        
        
        """
        parent = L[0]
        child = L[1]

        if len(L) == 3:
            new_child = L[2]
        else:
            new_child = child + "_st"
        st_ask = parent+"#"+child + "#" + new_child    
        st_br = tf2_ros.StaticTransformBroadcaster()    

        static_transformStamped = geometry_msgs.msg.TransformStamped() 
        static_transformStamped.transform = transf
        static_transformStamped.header.frame_id = parent
        static_transformStamped.child_frame_id = new_child
        static_transformStamped.header.stamp = rospy.Time.now()

        #print(static_transformStamped)

        #exit()
        suc  = False
        if not self.blockDicMod:
            suc = True
            st_br.sendTransform(static_transformStamped)  
            self.allstBR[st_ask] = [st_br,static_transformStamped]
        return(suc)

    def updateSTS(self):
        #self.blockDicMod = True
        try:
            for key in self.allstBR:
                L = self.allstBR[key]
                L[1].header.stamp = rospy.Time.now()
                L[0].sendTransform(L[1])
        except RuntimeError:
            pass
        self.blockDicMod = False


    def tfToStaticSrv(self, msg):
        """!

        Takes the last fixed parent->child transformation, and turn it into a static transformation parent->child_st.

        Two entries possibles : 
            * "help" : will give th necessary format
            * ...;[parent_frame_i]#[child_frame_i];[parent_frame_i+1]#[child_frame_i+1];... : will create the static
            transformations parent_frame_i->child_frame_i_st , parent_frame_i+1 -> child_frame_i+1_st , ...


        """
        response = ""
        success = True
        if (msg.data == "help"):
            response = "format : '...;[parent_frame_i]#[child_frame_i];[parent_frame_i+1]#[child_frame_i+1];...' "
        else:
            success,response = self.tfToStaticStr(msg.data)
        
        return(success,response)



    def tfToStaticStr(self, data):
        """!

        Takes the last fixed parent->child transformation, and turn it into a static transformation parent->child_st.

        Two entries possibles : 
            * "help" : will give th necessary format
            * ...;[parent_frame_i]#[child_frame_i];[parent_frame_i+1]#[child_frame_i+1];... : will create the static
            transformations parent_frame_i->child_frame_i_st , parent_frame_i+1 -> child_frame_i+1_st , ...


        """
        response = ""
        success = True
        LallTransfo = data.split(";")

        while success and len(LallTransfo) > 0:
            print(LallTransfo)
            L = LallTransfo[0].split("#")                

            del(LallTransfo[0])
            parent = L[0]
            child=  L[1]
            lt, success =  self.getPose(L[0],L[1])
            print(success)
            if (success):
                #print(lt)
                transf = self.L2T(lt)
                print(parent)
                print(child)
                # print(transf)
                # print(L)
                #exit()
                success = self.tfToStatic(transf,L)
                if not success:
                    response = "An update of the dictionnary was occuring, please retry."
            else:
                response = "Failed to find transform %s -> %s"%(parent, child)           
                
        return(success,response)

    def tfToStaticTS(self,tr):
        """!
        Static for transformStamped

        """
        L = [tr.header.frame_id,"", tr.child_frame_id]
        suc = self.tfToStatic(tr.transform,L)
        return(suc)


    def getPose(self,refG,refTrack,tries = 10):
        """!



        Will return the last transformation refTrack in refG, AS A LIST OBJECT
        
        poseGoal in order : [x,y,z, qx, qy, qz, qw]
        Returns as list

        """
        k = 0
        success = False
        poseGoal = []

        if (refG == refTrack):
            poseGoal = [
               0,0,0,0,0,0,1                                                                                
                ]
            success = True
        while (not rospy.is_shutdown() and not success and k < tries):
            try : 
                tlookup = geometry_msgs.msg.TransformStamped()
                tlookup = self.tfBuffer.lookup_transform(refG, refTrack, rospy.Time(0), rospy.Duration(1e-4))

                poseGoal = [
                tlookup.transform.translation.x,
                tlookup.transform.translation.y,
                tlookup.transform.translation.z,
                tlookup.transform.rotation.x,
                tlookup.transform.rotation.y,
                tlookup.transform.rotation.z,
                tlookup.transform.rotation.w                                                                                  
                ]

                self.lastHeaderTime = tlookup.header.stamp

                success = True
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                k+=1
        return(poseGoal,success)

    def areWaypointsClose(self,M1, M2, linthres = 1e-2, angthres = 0.1 ):
        """!
        
        Check whether two waypoints M1 and M2 (4x4) are 
        close or not.

        Params : 
            M1 and M2 : two waypoints expressed as homogeneous matrix (4x4)
            linthres : linear maximal distance (on each axis) in m.
            angthres : angular maximal distance (on each Euler angle) in rad.
        
        Returns : 
            closeLin (waypoints are close on linear part) AND closeAng (waypoints are close on angular part)

        NB : If you want to only check linear distance, set angthres to inf
        If you want to only check angular distance, set linthres to inf
        """
        
        diffMat = M1 @ np.linalg.inv(M2)
        # print(diffMat)
        homo_RM = np.eye(4,4)
        homo_RM[:3,:3] = diffMat[:3,:3]
        r,p,y = tf.transformations.euler_from_matrix(homo_RM)
        

        Ll = [diffMat[0,3],diffMat[1,3],diffMat[2,3]]
        La = [r,p,y]

        # print(Ll)
        # print(La)

        # rospy.loginfo("===")


        closeLin = True
        k = 0
        while k < len(Ll) and closeLin:
            if abs(Ll[k]) > linthres:
                closeLin = False
            k+=1

        closeAng = True
        k = 0
        while k < len(La) and closeAng:
            if abs(La[k]) > angthres:
                closeAng = False
            k+=1

        return(closeAng and closeLin)

    def O2M(self,obj):
        """!
        
        Automatically do the necessary transformation to a undefined object to matrix
        transform.

        The object could be : 
            * List
            * geometry_msgs.msg.Transform
            * geometry_msgs.msg.Pose
        
        """
        M = None

        if type(obj) is list:
            M = self.L2M(obj)

        elif type(obj) is geometry_msgs.msg.Transform:
            M = self.T2M(obj)
    
        elif type(obj) is geometry_msgs.msg.Pose:
            M = self.P2M(obj)
        else:
            M = obj

        return(M)

            
        
    def L2M(self,L):

        orientation_list = L[3:]
        ## normalize quaternion
        ML = np.array(orientation_list)
        orientation_list = list(ML/np.linalg.norm(ML))
        homoMat = tf.transformations.quaternion_matrix(orientation_list)        
        homoMat[0,3] = L[0]
        homoMat[1,3] = L[1]  
        homoMat[2,3] = L[2]

        return(homoMat)       
    
    def Lrpy2M(self,L):
        """!
        
        Use L = [x,y,z,rx,ry,rz] to get matrix

        """
        M = tf.transformations.euler_matrix(L[3],L[4],L[5])
        M[0,3] = L[0]
        M[1,3] = L[1]
        M[2,3] = L[2]

        return(M)

    def M2Lrpy(self,M):

        scale, shear, angles, trans, persp  = tf.transformations.decompose_matrix(M)
        L = [trans[0], trans[1], trans[2], angles[0], angles[1], angles[2] ]
        return(L)


    def M2L(self,mat):
        orientation_list = tf.transformations.quaternion_from_matrix(mat)
        ## normalize quaternion
        ML = np.array(orientation_list)
        orientation_list = list(ML/np.linalg.norm(ML))        
        L = [mat[0,3],
            mat[1,3],
            mat[2,3],
            orientation_list[0],
            orientation_list[1],
            orientation_list[2],
            orientation_list[3]                                    
        ]

        return(L)


    def T2M(self, transfo):
        L = self.T2L(transfo)
        return( self.L2M(L))

    def M2T(self,mat):
        L = self.M2L(mat)
        return(self.L2T(L))


    def L2T(self, L):
        tlookup = geometry_msgs.msg.Transform()
        tlookup.translation.x = L[0]
        tlookup.translation.y = L[1]
        tlookup.translation.z = L[2]
        tlookup.rotation.x = L[3]
        tlookup.rotation.y = L[4]
        tlookup.rotation.z = L[5]    
        tlookup.rotation.w = L[6]            
        return(tlookup)

    def Lrpy2T(self, L):
        M =self.Lrpy2M(L)
        tlookup = self.M2T(M)
        return(tlookup)
    
    def Lrpy2P(self, L):
        M =self.Lrpy2M(L)
        tlookup = self.M2P(M)
        return(tlookup)    

    def T2L(self, tlookup):
        L = [tlookup.translation.x,
        tlookup.translation.y,
        tlookup.translation.z,
        tlookup.rotation.x,
        tlookup.rotation.y,
        tlookup.rotation.z,    
        tlookup.rotation.w
        ]         
        return(L)

    def P2L(self, pose):
        L = [
        pose.position.x,
        pose.position.y,
        pose.position.z,
        pose.orientation.x,
        pose.orientation.y,
        pose.orientation.z,    
        pose.orientation.w,        
        ]
        return(L)

    def L2P(self, L):
        pose = geometry_msgs.msg.Pose()
        pose.position.x = L[0]
        pose.position.y = L[1]
        pose.position.z = L[2]
        pose.orientation.x = L[3]
        pose.orientation.y = L[4]
        pose.orientation.z = L[5]    
        pose.orientation.w = L[6]      
        return(pose)

    def P2T(self,pose):
        L = self.P2L(pose)
        return(self.L2T(L))

    def T2P(self,T):
        L = self.T2L(T)
        return(self.L2P(L))

    def P2M(self,pose):
        L = self.P2L(pose)
        return(self.L2M(L))

    def M2P(self,mat):
        L = self.M2L(mat)
        return(self.L2P(L))

    def isPoseEmpty(self,P):
        #L = self.P2L(P)
        # print("pose : ", P) 
        # print("pose is empty?:  ", self.P2L(P) == [0.0 for k in range(7)])
        return( self.P2L(P) == [0.0 for k in range(7)])

        
    def saveTransformToYaml(self,L, child, parent, yaml_name):
        calib_dat = dict(
            header=dict(frame_id=parent, child_frame_id=child),
            position=dict(
                x=float(L[0]),
                y=float(L[1]),
                z=float(L[2]),
            ),
            orientation=dict(
                x=float(L[3]),
                y=float(L[4]),
                z=float(L[5]),
                w=float(L[6]),
            ),
        )

        with open(
            yaml_name,
            "w",
        ) as file:
            yaml.dump(calib_dat, file, default_flow_style=False)



    def loadTransformFromYaml(self,yaml_path):
        transform = geometry_msgs.msg.TransformStamped()
        with open(yaml_path) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            transform.header.stamp = rospy.Time.now()
            transform.header.frame_id = data["header"]["frame_id"]
            transform.child_frame_id = data["header"]["child_frame_id"]
            transform.transform.translation.x = data["position"]["x"]
            transform.transform.translation.y = data["position"]["y"]
            transform.transform.translation.z = data["position"]["z"]
            transform.transform.rotation.x = data["orientation"]["x"]
            transform.transform.rotation.y = data["orientation"]["y"]
            transform.transform.rotation.z = data["orientation"]["z"]
            transform.transform.rotation.w = data["orientation"]["w"]
        return transform            